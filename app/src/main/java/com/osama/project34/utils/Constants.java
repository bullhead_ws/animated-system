package com.osama.project34.utils;

/**
 * Created by tripleheader on 1/27/17.
 * all the constants used in application
 */

public class Constants {
    public static final String GMAIL_HOST="imap.gmail.com";
    public static final String SHARED_PREFS_OAUTH="oauth";
    public static final String  STRING_ACCOUNT_SHARED_PREFS="account";
    public static final String DATA_ACTIVITY_INTENT_PERM="accountName";
    public static final String DATA_ACTIVITY_PERM_TOKEN="accessToken";


}
